# herocoder-jira-app

JavaScript / NodeJS app that uses Jira REST API to retrieve data from the Sample Project, and writes in human-readable form (to the console or a file) a list of components that don't have a 'component lead', along with the number of issues from this project which belongs to the component.

## Install dependencies

`npm i`

## Run App

`npm start`

## Run Tests

`npm test`
