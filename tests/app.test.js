import { expect, jest } from "@jest/globals";
import {
  getComponents,
  getNumberOfIssuesPerComponent,
} from "../src/services/jiraService";
import { runApp } from "../src/app";

test("get components", async () => {
  const components = await getComponents();
  expect(components).toHaveLength(5);
});

test("get number of issues per component", async () => {
  const components = await getComponents();
  const responses = await getNumberOfIssuesPerComponent(components);
  expect(responses).toHaveLength(3);
});

test("check app run successfully", async () => {
  console.debug = jest.fn();
  await runApp();
  expect(console.debug.mock.calls[0][0]).toBe("Execution finished.");
});
