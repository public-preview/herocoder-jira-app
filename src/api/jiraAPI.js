import axios from "axios";

export const jiraClient = axios.create({
  baseURL: "https://herocoders.atlassian.net/rest/api/3",
});
