import {
  getComponents,
  getNumberOfIssuesPerComponent,
  printResult,
} from "./services/jiraService.js";

export const runApp = async () => {
  const components = await getComponents();
  const responses = await getNumberOfIssuesPerComponent(components);
  printResult(responses);
};

runApp();
