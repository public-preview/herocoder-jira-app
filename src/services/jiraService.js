
import axios from "axios";
import { table } from "table";
import { jiraClient } from "../api/jiraAPI.js";

/**
 * Get all components for project
 *
 * @returns components[]
 */
export const getComponents = async () => {
  try {
    const { data } = await jiraClient.get("/project/SP/components");

    return data;
  } catch (error) {
    console.error(`Unable to fetch Components. ${error.message}`);
    throw error;
  }
};

/**
 * Getting issues per component for which lead was not assign
 *
 * @param {*} components
 * @returns responses[]
 */
export const getNumberOfIssuesPerComponent = async (components) => {
  try {
    const componentsWithNoLead = components.filter(
      (component) => !component.lead
    );

    const requests = componentsWithNoLead.map(async (component) => {
      return {
        response: await jiraClient.get("/search", {
          params: {
            jql: `project = SP  AND component = ${component.name}`,
          },
        }),
        componentName: component.name,
      };
    });

    return axios.all(requests);
  } catch (error) {
    console.error(
      `Unable to fetch issues for ${components} component. ${error.message}`
    );
    throw error;
  }
};

/**
 * Print result in table view format to console
 *
 * @param {*} responses
 */
export const printResult = (responses) => {
  let tableData = [["Component Name", "Number of Issues"]];
  responses.forEach((res) => {
    tableData.push([res.componentName, res.response.data.total]);
  });

  const config = {
    header: {
      alignment: "center",
      content: "SP Project",
    },
    columns: [
      { alignment: "left" },
      { alignment: "center" }
    ],
  };

  console.debug('Execution finished.');
  console.log(table(tableData, config));
};
